# import math
import pygame
from planet import Planet

pygame.init()

WIDTH, HEIGHT = 800, 800

WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Planet simulation")

YELLOW = (255, 255, 0)
BLUE = (0, 0, 255)
RED = (255, 0, 0)
GRAY = (80, 78, 81)
WHITE = (255, 255, 255)

def main():
    run = True
    clock = pygame.time.Clock()


    sun = Planet(0, 0, 30, YELLOW, 1.98892e30, True)
    earth = Planet(-1 * Planet.AU, 0, 16, BLUE, 5.9742e24)
    earth.y_vel = 29.783e3
    mars = Planet(-1.524 * Planet.AU, 0, 12, RED, 6.39e23)
    mars.y_vel = 24.077e3
    mercury = Planet(0.387 * Planet.AU, 0, 8, GRAY, 3.3e23)
    mercury.y_vel = -47.4e3
    venus = Planet(0.723 * Planet.AU, 0, 14, WHITE, 4.8685e24)
    venus.y_vel = -35.02e3

    planets = [sun, earth, mars, mercury, venus]

    while run:
        # Cap the loop at 60 Hz
        clock.tick(60)
        WINDOW.fill((0, 0, 0))

        for event in pygame.event.get():
            if event.type == pygame.QUIT: run = False

        for planet in planets:
            planet.update_position(planets)
            planet.draw(WINDOW)

        pygame.display.update()

    pygame.quit()

main()
